<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <meta http-equiv="X-UA-Compatible" content="ie=8 chrome=1" />
    <link href="../../lib/ligerUI/skins/ext/css/ligerui-all.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/input.css" rel="stylesheet" />

    <script src="../../lib/jquery/jquery-1.3.2.min.js" type="text/javascript"></script>
    <script src="../../lib/ligerUI/js/plugins/ligerLayout.js" type="text/javascript"></script>
    <script src="../../lib/ligerUI/js/plugins/ligerTab.js" type="text/javascript"></script>

    <script src="../../lib/ligerUI/js/plugins/ligerGrid.js" type="text/javascript"></script>
    <script src="../../lib/ligerUI/js/plugins/ligerForm.js" type="text/javascript"></script>
    <script src="../../lib/ligerUI/js/plugins/ligerCheckBox.js" type="text/javascript"></script>
    <script src="../../lib/ligerUI/js/plugins/ligerComboBox.js" type="text/javascript"></script>
    <script src="../../lib/ligerUI/js/plugins/ligerDateEditor.js" type="text/javascript"></script>
    <script src="../../lib/ligerUI/js/plugins/ligerRadio.js" type="text/javascript"></script>
    <script src="../../lib/ligerUI/js/plugins/ligerTextBox.js" type="text/javascript"></script>
    <script src="../../lib/ligerUI/js/plugins/ligerSpinner.js" type="text/javascript"></script>
    <script src="../../lib/ligerUI/js/plugins/ligerTree.js" type="text/javascript"></script>
    <script src="../../lib/ligerUI/js/plugins/ligerDialog.js" type="text/javascript"></script>
    <script src="../../lib/ligerUI/js/plugins/ligerDrag.js" type="text/javascript"></script>
    <script src="../../lib/ligerUI/js/plugins/ligerResizable.js" type="text/javascript"></script>
    <script src="../../lib/ligerUI/js/plugins/ligerLayout.js" type="text/javascript"></script>
    <script src="../../lib/ligerUI/js/plugins/ligerTip.js" type="text/javascript"></script>
    <script src="../../lib/jquery.form.js" type="text/javascript"></script>
    <script src="../../lib/ligerUI/js/plugins/ligerMenu.js" type="text/javascript"></script>
    <script src="../../lib/ligerUI/js/plugins/ligerToolBar.js" type="text/javascript"></script>
    <script src="../../JS/XHD.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {

            initLayout();
            $(window).resize(function () {
                initLayout();
            });
            $("#maingrid4").ligerGrid({
                columns: [
                    { display: '序号', name: 'n', width: 50 },
                     {
                         display: '任务名称', name: 'task_title', width: 200, render: function (item, i) {
                             var html = "<a href='javascript:void(0)' onclick=view(9," + item.id + ")>";
                             if (item.task_title)
                                 html += item.task_title;
                             html += "</a>";
                             return html;
                         }
                     },
                    { display: '任务类别', name: 'task_type', width: 80 },
                    { display: '执行人', name: 'executive', width: 80 },
                    { display: '指派人', name: 'assign', width: 80 },
                    {
                        display: '执行时间', name: 'executive_time', width: 90, render: function (item) {
                            var d = formatTimebytype(item.executive_time, 'yyyy-MM-dd');
                            var diff = DateDiff(d);
                            if (diff < 0)
                                return "<div style='background:#f00;color:#fff;'>" + d + "</div>";
                            else return d;
                        }
                    },
                    {
                        display: '任务状态', name: 'task_status_id', width: 80, render: function (item, i) {
                            if (item.task_status_id == 0)
                                return "<div style='background:#0f0;'>进行中</div>";
                            else if (item.task_status_id == 1)
                                return "<div style='background:#f00;color:#fff;'>已完成</div>";
                            else if (item.task_status_id == 2)
                                return "<div style='background:#00f;color:#0ff;'>已中止</div>";
                            else
                                return "";

                        }
                    },
                    {
                        display: '优先级', name: 'priority_id', width: 60, render: function (item, i) {
                            if (item.priority_id == 0)
                                return "高";
                            else if (item.priority_id == 1)
                                return "中";
                            else
                                return "低";
                        }
                    },
                    {
                        display: '相关客户', name: 'customer', width: 200, render: function (item) {
                            var html = "<a href='javascript:void(0)' onclick=view(1," + item.customer_id + ")>";
                            if (item.customer)
                                html += item.customer;
                            html += "</a>";
                            return html;
                        }
                    },
                {
                    display: '创建时间', name: 'create_time', width: 150, render: function (item) {
                        return formatTime(item.create_time);
                    }
                }
                ],
                //fixedCellHeight:false,
                
                dataAction: 'server', pageSize: 30, pageSizeOptions: [20, 30, 50, 100],
                url: "MSG_Task.grid.xhd?rnd=" + Math.random(),
                width: '100%', height: '100%',
                heightDiff: -1,
                onRClickToSelect: true,
                onContextmenu: function (parm, e) {
                    actionCustomerID = parm.data.id;
                    menu.show({ top: e.pageY, left: e.pageX });
                    return false;
                }
            });

            $("#grid").height(document.documentElement.clientHeight - $(".toolbar").height());

            $('form').ligerForm();
            toolbar();
        });

        //工具条实例化
        function toolbar() {
            $.getJSON("toolbar.GetSys.xhd?mid=96&rnd=" + Math.random(), function (data, textStatus) {
                //alert(data);
                var items = [];
                var arr = data.Items;
                for (var i = 0; i < arr.length; i++) {
                    arr[i].icon = "../../" + arr[i].icon;
                    items.push(arr[i]);
                }
                items.push({
                    type: 'serchbtn',
                    text: '高级搜索',
                    icon: '../../images/search.gif',
                    disable: true,
                    click: function () {
                        serchpanel();
                    }
                });

                $("#toolbar").ligerToolBar({
                    items: items
                });
                menu = $.ligerMenu({
                    width: 120, items: getMenuItems(data)
                });

                $("#maingrid4").ligerGetGridManager().onResize();
            });
        }
        function initSerchForm() {
            $('#executive_employee').ligerComboBox({
                width: 120,
                onBeforeOpen: f_selectExecutive
            });

            $('#assign_employee').ligerComboBox({
                width: 120,
                onBeforeOpen: f_selectAssign
            });

        }
        function serchpanel() {
            initSerchForm();
            if ($(".az").css("display") == "none") {
                $("#grid").css("margin-top", $(".az").height() + "px");
                $("#maingrid4").ligerGetGridManager().onResize();
            } else {
                $("#grid").css("margin-top", "0px");
                $("#maingrid4").ligerGetGridManager().onResize();
            }
            $("#company").focus();
        }

        $(document).keydown(function (e) {
            if (e.keyCode == 13 && e.target.applyligerui) {
                doserch();
            }
        });
        //查询
        function doserch() {
            var sendtxt = "&rnd=" + Math.random();
            var serchtxt = $("#serchform :input").fieldSerialize() + sendtxt;
            //alert(serchtxt);           
            var manager = $("#maingrid4").ligerGetGridManager();

            manager.GetDataByURL("MSG_Task.grid.xhd?" + serchtxt);
        }

        //重置
        function doclear() {
            $("input:hidden", "#serchform").val("");
            $("input:text", "#serchform").val("");
            $(".l-selected").removeClass("l-selected");
        }

        function f_selectExecutive() {
            $.ligerDialog.open({
                title: '选择联系人', width: 650, height: 300, url: '../../hr/getemp_auth.aspx?auth=0', buttons: [
                    { text: '确定', onclick: f_selectExecutiveOK },
                    { text: '取消', onclick: function (item, dialog) { dialog.close(); } }
                ]
            });
            return false;
        }
        function f_selectExecutiveOK(item, dialog) {
            var data = dialog.frame.f_select();
            if (!data) {
                alert('请选择行!');
                return;
            }
            $("#executive_employee").val(data.name);
            $("#executive_employee_val").val(data.ID);

            dialog.close();
        }
        function f_selectAssign() {
            $.ligerDialog.open({
                title: '选择联系人', width: 650, height: 300, url: '../../hr/getemp_auth.aspx', buttons: [
                    { text: '确定', onclick: f_selectAssignOK },
                    { text: '取消', onclick: function (item, dialog) { dialog.close(); } }
                ]
            });
            return false;
        }
        function f_selectAssignOK(item, dialog) {
            var data = dialog.frame.f_select();
            if (!data) {
                alert('请选择行!');
                return;
            }
            $("#assign_employee").val(data.name);
            $("#assign_employee_val").val(data.ID);

            dialog.close();
        }

        //添加
        function add() {
            f_openWindow("personal/task/task_list_add.aspx", "新增任务", 770, 450, f_save);
        }

        //修改
        function edit() {
            var manager = $("#maingrid4").ligerGetGridManager();
            var row = manager.getSelectedRow();
            if (row) {
                f_openWindow('personal/task/task_list_add.aspx?id=' + row.id, "修改任务", 770, 450, f_save);
            }
            else {
                $.ligerDialog.warn('请选择行！');
            }
        }


        //删除
        function del() {
            var manager = $("#maingrid4").ligerGetGridManager();
            var row = manager.getSelectedRow();
            if (row) {
                $.ligerDialog.confirm("删除后不能恢复，且任务下所有跟进都将删除，\n您确定要删除？", function (yes) {
                    if (yes) {
                        $.ajax({
                            url: "MSG_Task.del.xhd", type: "get",
                            data: { id: row.id, rnd: Math.random() },
                            success: function (responseText) {
                                if (responseText == "true") {
                                    f_reload();
                                }
                                else if (responseText == "auth") {
                                    top.$.ligerDialog.error('权限不够！');
                                }
                                else {
                                    top.$.ligerDialog.error('删除失败！');
                                }

                            },
                            error: function () {
                                top.$.ligerDialog.error('删除失败！');
                            }
                        });
                    }
                })
            }
            else {
                $.ligerDialog.warn("请选择客户");
            }
        }

        //保存按钮
        function f_save(item, dialog) {
            var issave = dialog.frame.f_save();
            if (issave) {
                dialog.close();
                top.$.ligerDialog.waitting('数据保存中,请稍候...');
                $.ajax({
                    url: "MSG_Task.save.xhd", type: "POST",
                    data: issave,
                    success: function (responseText) {
                        top.$.ligerDialog.closeWaitting();
                        f_reload();
                    },
                    error: function () {
                        top.$.ligerDialog.closeWaitting();
                        top.$.ligerDialog.error('操作失败！');
                    }
                });

            }
        }

        //刷新页面，重新加载Grid
        function f_reload() {
            $("#maingrid4").ligerGetGridManager().loadData(true);
        };


    </script>
</head>
<body>
    <form id="form1" onsubmit="return false">

        <div id="toolbar"></div>
        <div id="grid">
            <div id="maingrid4" style="margin: -1px; min-width: 800px;"></div>
        </div>
    </form>
    <div class="az">
        <form id='serchform'>
            <table style='width: 960px' class="bodytable1">
                <tr>
                    <td>
                        <div style='float: right; text-align: right; width: 60px;'>任务名称：</div>
                    </td>
                    <td>
                        <input type='text' id='taskname' name='taskname' ltype='text' ligerui='{width:120}' /></td>


                    <td>
                        <div style='float: right; text-align: right; width: 60px;'>执行时间：</div>
                    </td>
                    <td>
                        <div style='float: left; width: 100px;'>
                            <input type='text' id='startdate' name='startdate' ltype='date' ligerui='{width:97}' />
                        </div>
                        <div style='float: left; width: 98px;'>
                            <input type='text' id='enddate' name='enddate' ltype='date' ligerui='{width:96}' />
                        </div>
                    </td>
                    <td>
                        <div style='float: right; text-align: right; width: 60px;'>执行人：</div>
                    </td>
                    <td>

                        <input type='text' id='executive_employee' name='executive_employee' />
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                        <div style='float: right; text-align: right; width: 60px;'>任务类别：</div>
                    </td>
                    <td>
                        <input id='task_status' name="task_status" type='text' ltype="select" ligerui="{width:120,data:[{id:0,text:'进行中'},{id:1,text:'已完成'},{id:2,text:'已终止'}] }" /></td>


                    <td>
                        <div style='float: right; text-align: right; width: 60px;'>创建时间：</div>
                    </td>
                    <td>
                        <div style='float: left; width: 100px;'>
                            <input type='text' id='startcreate' name='startcreate' ltype='date' ligerui='{width:97}' />
                        </div>
                        <div style='float: left; width: 98px;'>
                            <input type='text' id='endcreate' name='endcreate' ltype='date' ligerui='{width:96}' />
                        </div>
                    </td>
                    <td>
                        <div style='float: right; text-align: right; width: 60px;'>指派人：</div>
                    </td>
                    <td>

                        <input type='text' id='assign_employee' name='assign_employee' />
                    </td>
                    <td>

                        <input id='Button2' type='button' value='重置' style='height: 24px; width: 80px;'
                            onclick=" doclear() " />
                        <input id='Button1' type='button' value='搜索' style='height: 24px; width: 80px;' onclick=" doserch() " /></td>
                </tr>
            </table>
        </form>
    </div>
</body>
</html>
