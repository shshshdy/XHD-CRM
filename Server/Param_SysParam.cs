﻿/*
* Param_SysParam.cs
*
* 功 能： N/A
* 类 名： Param_SysParam
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2015-06-23 18:38:21    黄润伟    
*
* Copyright (c) 2015 www.xhdcrm.com   All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：黄润伟                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/

using System.Data;
using System.Text;
using System.Web;
using XHD.BLL;
using XHD.Common;
using XHD.Controller;

namespace XHD.Server
{
    public class Param_SysParam
    {
        public static BLL.Param_SysParam psp = new BLL.Param_SysParam();
        public static Model.Param_SysParam model = new Model.Param_SysParam();

        public HttpContext Context;
        public int emp_id;
        public string emp_name;
        public Model.hr_employee employee;
        public HttpRequest request;
        public string uid;


        public Param_SysParam()
        {
        }

        public Param_SysParam(HttpContext context)
        {
            Context = context;
            request = context.Request;

            var userinfo = new User_info();
            employee = userinfo.GetCurrentEmpInfo(context);

            emp_id = employee.ID;
            emp_name = PageValidate.InputText(employee.name, 50);
            uid = PageValidate.InputText(employee.uid, 50);
        }

        public string GetApp()
        {
            var cpst = new Param_SysParam_Type();
            DataSet ds = cpst.GetList(0, "", "params_order");

            var str = new StringBuilder();
            str.Append("[");
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                str.Append("{id:" + ds.Tables[0].Rows[i]["id"] + ",pid:0,text:'" + ds.Tables[0].Rows[i]["params_name"] +
                           "'},");
            }
            str.Replace(",", "", str.Length - 1, 1);
            str.Append("]");
            return str.ToString();
        }

        public string GetParams(int id)
        {
            DataSet ds = psp.GetList(0, " parentid=" + id, "params_order,params_name");
            string dt = GetGridJSON.DataTableToJSON(ds.Tables[0]);
            return dt;
        }

        //combo
        public string combo()
        {
            string id = PageValidate.InputText(request["parentid"], 50);
            DataSet ds = psp.GetList(0, " parentid = " + id, "params_order,params_name");

            var str = new StringBuilder();

            str.Append("[");
            //str.Append("{id:0,text:'无'},");
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                str.Append("{id:" + ds.Tables[0].Rows[i]["id"] + ",text:'" + ds.Tables[0].Rows[i]["params_name"] + "'},");
            }
            str.Replace(",", "", str.Length - 1, 1);
            str.Append("]");

            return str.ToString();
        }

        //Form JSON
        public string form()
        {
            string id = PageValidate.InputText(request["paramid"], 50);
            DataSet ds;
            string dt;
            if (PageValidate.IsNumber(id))
            {
                ds = psp.GetList("id=" + int.Parse(request["paramid"]));

                dt = DataToJson.DataToJSON(ds);
            }
            else
            {
                dt = "{}";
            }

            return dt;
        }

        //save
        public void save()
        {
            model.params_name = PageValidate.InputText(request["T_param_name"], 255);
            model.params_order = int.Parse(request["T_param_order"]);

            string id = request["paramid"];

            if (PageValidate.IsNumber(id))
            {
                model.id = int.Parse(id);
                psp.Update(model);
            }
            else
            {
                model.parentid = int.Parse(request["parentid"]);
                psp.Add(model);
            }
        }

        //del
        public string del(int paramid)
        {
            int id = paramid;
            int parentid = int.Parse(request["parentid"]);
            var customer = new BLL.CRM_Customer();
            var follow = new BLL.CRM_Follow();
            var order = new BLL.CRM_order();
            var invoice = new BLL.CRM_invoice();
            var receive = new BLL.CRM_receive();
            var task = new BLL.MSG_Task();           

            DataSet ds = null;

            switch (parentid)
            {
                case 8:
                    ds = customer.GetList("industry_id=" + id);
                    break;
                case 1:
                    ds = customer.GetList("CustomerType_id=" + id);
                    break;
                case 2:
                    ds = customer.GetList("CustomerLevel_id=" + id);
                    break;
                case 3:
                    ds = customer.GetList("CustomerSource_id=" + id);
                    break;
                case 4:
                    ds = follow.GetList("Follow_Type_id=" + id);
                    break;
                case 5:
                    ds = order.GetList("pay_type_id=" + id);
                    if (ds.Tables[0].Rows.Count == 0) ds = receive.GetList("Pay_type_id=" + id);
                    break;
                case 6:
                    ds = order.GetList("Order_status_id=" + id);
                    break;
                case 7:
                    ds = invoice.GetList("invoice_type_id=" + id);
                    break;
                case 9:
                    ds = follow.GetList("Follow_aim_id=" + id);
                    break;
                case 10:
                    ds = task.GetList("task_type_id=" + id);
                    break;
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                return ("false:data");
            }

            bool isdel = psp.Delete(id);
            if (isdel)
            {
                return ("true");
            }
            return ("false");
        }

        public string validate()
        {
            string param_name = PageValidate.InputText(request["T_param_name"], 50);
            string parentid = PageValidate.InputText(request["parentid"], 50);
            string cid = PageValidate.InputText(request["T_cid"], 50);
            if (string.IsNullOrEmpty(cid) || cid == "null")
                cid = "0";

            DataSet ds =
                psp.GetList(string.Format(" params_name='{0}' and parentid={1} and id!={2} ", param_name, parentid, cid));
            //context.Response.Write(" Count:" + ds.Tables[0].Rows.Count);

            if (ds.Tables[0].Rows.Count > 0)
            {
                return ("false");
            }
            return ("true");
        }
    }
}