﻿/*
* CRM_product_category.cs
*
* 功 能： N/A
* 类 名： CRM_product_category
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2015-06-24 09:57:29    黄润伟    
*
* Copyright (c) 2015 www.xhdcrm.com   All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：黄润伟                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/

using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using XHD.DBUtility;

//Please add references

namespace XHD.DAL
{
    /// <summary>
    ///     数据访问类:CRM_product_category
    /// </summary>
    public class CRM_product_category
    {
        #region  BasicMethod

        /// <summary>
        ///     增加一条数据
        /// </summary>
        public int Add(Model.CRM_product_category model)
        {
            var strSql = new StringBuilder();
            strSql.Append("insert into CRM_product_category(");
            strSql.Append("product_category,parentid,product_icon,isDelete,Delete_id,Delete_time)");
            strSql.Append(" values (");
            strSql.Append("@product_category,@parentid,@product_icon,@isDelete,@Delete_id,@Delete_time)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters =
            {
                new SqlParameter("@product_category", SqlDbType.VarChar, 250),
                new SqlParameter("@parentid", SqlDbType.Int, 4),
                new SqlParameter("@product_icon", SqlDbType.VarChar, 250),
                new SqlParameter("@isDelete", SqlDbType.Int, 4),
                new SqlParameter("@Delete_id", SqlDbType.Int, 4),
                new SqlParameter("@Delete_time", SqlDbType.DateTime)
            };
            parameters[0].Value = model.product_category;
            parameters[1].Value = model.parentid;
            parameters[2].Value = model.product_icon;
            parameters[3].Value = model.isDelete;
            parameters[4].Value = model.Delete_id;
            parameters[5].Value = model.Delete_time;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            return Convert.ToInt32(obj);
        }

        /// <summary>
        ///     更新一条数据
        /// </summary>
        public bool Update(Model.CRM_product_category model)
        {
            var strSql = new StringBuilder();
            strSql.Append("update CRM_product_category set ");
            strSql.Append("product_category=@product_category,");
            strSql.Append("parentid=@parentid,");
            strSql.Append("product_icon=@product_icon");
            strSql.Append(" where id=@id");
            SqlParameter[] parameters =
            {
                new SqlParameter("@product_category", SqlDbType.VarChar, 250),
                new SqlParameter("@parentid", SqlDbType.Int, 4),
                new SqlParameter("@product_icon", SqlDbType.VarChar, 250),
                new SqlParameter("@id", SqlDbType.Int, 4)
            };
            parameters[0].Value = model.product_category;
            parameters[1].Value = model.parentid;
            parameters[2].Value = model.product_icon;
            parameters[3].Value = model.id;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        ///     删除一条数据
        /// </summary>
        public bool Delete(int id)
        {
            var strSql = new StringBuilder();
            strSql.Append("delete from CRM_product_category ");
            strSql.Append(" where id=@id");
            SqlParameter[] parameters =
            {
                new SqlParameter("@id", SqlDbType.Int, 4)
            };
            parameters[0].Value = id;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        ///     批量删除数据
        /// </summary>
        public bool DeleteList(string idlist)
        {
            var strSql = new StringBuilder();
            strSql.Append("delete from CRM_product_category ");
            strSql.Append(" where id in (" + idlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        ///     获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            var strSql = new StringBuilder();
            strSql.Append("select id,product_category,parentid,product_icon,isDelete,Delete_id,Delete_time ");
            strSql.Append(" FROM CRM_product_category ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        ///     获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            var strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top);
            }
            strSql.Append(" id,product_category,parentid,product_icon,isDelete,Delete_id,Delete_time ");
            strSql.Append(" FROM CRM_product_category ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        ///     分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder, out string Total)
        {
            var strSql_grid = new StringBuilder();
            var strSql_total = new StringBuilder();
            strSql_total.Append(" SELECT COUNT(id) FROM CRM_product_category ");
            strSql_grid.Append("SELECT ");
            strSql_grid.Append("      id,product_category,parentid,product_icon,isDelete,Delete_id,Delete_time ");
            strSql_grid.Append(
                " FROM ( SELECT id,product_category,parentid,product_icon,isDelete,Delete_id,Delete_time, ROW_NUMBER() OVER( Order by " +
                filedOrder + " ) AS n from CRM_product_category");
            if (strWhere.Trim() != "")
            {
                strSql_grid.Append(" WHERE " + strWhere);
                strSql_total.Append(" WHERE " + strWhere);
            }
            strSql_grid.Append("  ) as w1  ");
            strSql_grid.Append("WHERE n BETWEEN " + PageSize*(PageIndex - 1) + " AND " + PageSize*PageIndex);
            strSql_grid.Append(" ORDER BY " + filedOrder);
            Total = DbHelperSQL.Query(strSql_total.ToString()).Tables[0].Rows[0][0].ToString();
            return DbHelperSQL.Query(strSql_grid.ToString());
        }

        #endregion  BasicMethod

        #region  ExtensionMethod

        #endregion  ExtensionMethod
    }
}