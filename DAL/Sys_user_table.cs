﻿/*
* Sys_user_table.cs
*
* 功 能： N/A
* 类 名： Sys_user_table
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2015/8/1 10:38:12    黄润伟    
*
* Copyright (c) 2015 www.xhdcrm.com  www.xhdoa.com. All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：小黄豆                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/

using System.Data;
using System.Data.SqlClient;
using System.Text;
using XHD.DBUtility;

//Please add references

namespace XHD.DAL
{
    /// <summary>
    ///     数据访问类:Sys_user_table
    /// </summary>
    public class Sys_user_table
    {
        #region  BasicMethod

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(Model.Sys_user_table model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from Sys_user_table");
            strSql.Append(" where ");
            strSql.Append("emp_id=@emp_id ");
            strSql.Append("and menu_id=@menu_id ");
            strSql.Append("and column_id=@column_id ");

            SqlParameter[] parameters =
            {
                new SqlParameter("@emp_id", SqlDbType.Int, 4),
                new SqlParameter("@menu_id", SqlDbType.Int, 4),
                new SqlParameter("@column_id", SqlDbType.Int, 4)
            };
            parameters[0].Value = model.emp_id;
            parameters[1].Value = model.menu_id;
            parameters[2].Value = model.column_id;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }

        /// <summary>
        ///     增加一条数据
        /// </summary>
        public bool Add(Model.Sys_user_table model)
        {
            var strSql = new StringBuilder();
            strSql.Append("insert into Sys_user_table(");
            strSql.Append("emp_id,menu_id,column_id,isHide,column_width)");
            strSql.Append(" values (");
            strSql.Append("@emp_id,@menu_id,@column_id,@isHide,@column_width)");
            SqlParameter[] parameters =
            {
                new SqlParameter("@emp_id", SqlDbType.Int, 4),
                new SqlParameter("@menu_id", SqlDbType.Int, 4),
                new SqlParameter("@column_id", SqlDbType.Int, 4),
                new SqlParameter("@isHide", SqlDbType.Int, 4),
                new SqlParameter("@column_width", SqlDbType.Int, 4)
            };
            parameters[0].Value = model.emp_id;
            parameters[1].Value = model.menu_id;
            parameters[2].Value = model.column_id;
            parameters[3].Value = model.isHide;
            parameters[4].Value = model.column_width;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        ///     更新一条数据
        /// </summary>
        public bool UpdateWidth(Model.Sys_user_table model)
        {
            var strSql = new StringBuilder();
            strSql.Append("update Sys_user_table set ");
            strSql.Append("column_width=@column_width");
            strSql.Append(" where ");
            strSql.Append("emp_id=@emp_id ");
            strSql.Append("and menu_id=@menu_id ");
            strSql.Append("and column_id=@column_id ");

            SqlParameter[] parameters =
            {
                new SqlParameter("@emp_id", SqlDbType.Int, 4),
                new SqlParameter("@menu_id", SqlDbType.Int, 4),
                new SqlParameter("@column_id", SqlDbType.Int, 4),
                new SqlParameter("@column_width", SqlDbType.Int, 4)
            };
            parameters[0].Value = model.emp_id;
            parameters[1].Value = model.menu_id;
            parameters[2].Value = model.column_id;
            parameters[3].Value = model.column_width;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        ///     更新一条数据
        /// </summary>
        public bool UpdateHide(Model.Sys_user_table model)
        {
            var strSql = new StringBuilder();
            strSql.Append("update Sys_user_table set ");
            strSql.Append("isHide=@isHide");
            strSql.Append(" where ");
            strSql.Append("emp_id=@emp_id ");
            strSql.Append("and menu_id=@menu_id ");
            strSql.Append("and column_id=@column_id ");

            SqlParameter[] parameters =
            {
                new SqlParameter("@emp_id", SqlDbType.Int, 4),
                new SqlParameter("@menu_id", SqlDbType.Int, 4),
                new SqlParameter("@column_id", SqlDbType.Int, 4),
                new SqlParameter("@isHide", SqlDbType.Int, 4)
            };
            parameters[0].Value = model.emp_id;
            parameters[1].Value = model.menu_id;
            parameters[2].Value = model.column_id;
            parameters[3].Value = model.isHide;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        ///     删除一条数据
        /// </summary>
        public bool Delete(Model.Sys_user_table model)
        {
            //该表无主键信息，请自定义主键/条件字段
            var strSql = new StringBuilder();
            strSql.Append("delete from Sys_user_table ");
            strSql.Append(" where ");
            strSql.Append("emp_id=@emp_id ");
            strSql.Append("and menu_id=@menu_id ");
            strSql.Append("and column_id=@column_id ");

            SqlParameter[] parameters =
            {
                new SqlParameter("@emp_id", SqlDbType.Int, 4),
                new SqlParameter("@menu_id", SqlDbType.Int, 4),
                new SqlParameter("@column_id", SqlDbType.Int, 4)
            };
            parameters[0].Value = model.emp_id;
            parameters[1].Value = model.menu_id;
            parameters[2].Value = model.column_id;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        ///     获得数据列表
        /// </summary>
        public DataSet GetList(Model.Sys_user_table model)
        {
            var strSql = new StringBuilder();
            strSql.Append("select emp_id,menu_id,column_id,isHide,column_width ");
            strSql.Append(" FROM Sys_user_table ");
            strSql.Append(" where ");
            strSql.Append("emp_id=@emp_id ");
            strSql.Append("and menu_id=@menu_id ");

            SqlParameter[] parameters =
            {
                new SqlParameter("@emp_id", SqlDbType.Int, 4),
                new SqlParameter("@menu_id", SqlDbType.Int, 4)
            };
            parameters[0].Value = model.emp_id;
            parameters[1].Value = model.menu_id;

            return DbHelperSQL.Query(strSql.ToString(),parameters);
        }

        #endregion  BasicMethod

        #region  ExtensionMethod

        #endregion  ExtensionMethod
    }
}