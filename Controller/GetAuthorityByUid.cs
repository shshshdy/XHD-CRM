﻿/*
* GetAuthorityByUid.cs
*
* 功 能： N/A
* 类 名： GetAuthorityByUid
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2015-06-23 18:38:21    黄润伟    
*
* Copyright (c) 2015 www.xhdcrm.com   All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：黄润伟                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/

using System.Linq;
using System.Data;
using XHD.BLL;

namespace XHD.Controller
{
    public class GetAuthorityByUid
    {
        public GetAuthorityByUid()
        {
        }

        public string GetAuthority(string uid, string RequestType)
        {
            switch (RequestType)
            {
                case "Apps":
                    return GetApp(uid);
                case "Menus":
                    return GetMenus(uid);
                    //case "CostMenus":
                    //    return GetCost(uid);
                    //case "Estate":
                    //    return GetEstate(uid);
            }
            return "";
        }

        public bool GetBtnAuthority(string uid, string btnid)
        {
            if (!string.IsNullOrEmpty(uid) && !string.IsNullOrEmpty(btnid))
            {
                int istrue = GetBtn(uid).IndexOf("b" + btnid + ",");
                if (istrue == -1)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return false;
            }
        }

        public string GetAppAuthority(string uid, string appid)
        {
            if (!string.IsNullOrEmpty(uid) && !string.IsNullOrEmpty(appid))
            {
                int istrue = GetApp(uid).IndexOf("a" + appid + ",");
                if (istrue == -1)
                {
                    return "false";
                }
                else
                {
                    return "true";
                }
            }
            else
            {
                return "false";
            }
        }


        private string GetRoleidByUID(string uid)
        {
            if (string.IsNullOrEmpty(uid))
            {
                return "(0)";
            }
            else
            {
                var rm = new Sys_role_emp();
                DataSet ds = rm.GetList("empID=" + int.Parse(uid));
                if (ds.Tables[0].Rows.Count > 0)
                {
                    string RoleIDs = "(";
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        RoleIDs += ds.Tables[0].Rows[i]["RoleID"].ToString() + ",";
                    }
                    RoleIDs = RoleIDs.Substring(0, RoleIDs.Length - 1);
                    RoleIDs += ")";
                    return RoleIDs;
                }
                else
                {
                    return "(0)";
                }
            }
        }

        private string GetApp(string empID)
        {
            if (string.IsNullOrEmpty(empID))
            {
                return "(0)";
            }
            else
            {
                var auth = new Sys_authority();
                string RoleIDs = GetRoleidByUID(empID);

                DataSet ds = auth.GetList("Menu_ids!='' and Role_ID in " + RoleIDs);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    string Apps = "(";
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        Apps += ds.Tables[0].Rows[i]["App_ids"] + ",";
                    }
                    Apps = Apps.Substring(0, Apps.Length - 1);
                    Apps += ")";
                    return Apps;
                }
                else
                {
                    return "(0)";
                }
            }
        }

        public string GetMenus(string empID)
        {
            if (string.IsNullOrEmpty(empID))
            {
                return "(0)";
            }
            else
            {
                var auth = new Sys_authority();
                string RoleIDs = GetRoleidByUID(empID);

                DataSet ds = auth.GetList("Role_ID in " + RoleIDs);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    string Menus = "(";
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        Menus += ds.Tables[0].Rows[i]["Menu_ids"];
                    }
                    Menus = Menus.Trim().TrimEnd(',');
                    Menus = string.Join(",", Menus.Split(',').Distinct().ToArray());
                    Menus += ")";
                    return Menus.Replace("m", "");
                }
                else
                {
                    return "(0)";
                }
            }
        }

        private string GetBtn(string empID)
        {
            if (string.IsNullOrEmpty(empID))
            {
                return "(0)";
            }
            else
            {
                var auth = new Sys_authority();
                string RoleIDs = GetRoleidByUID(empID);

                DataSet ds = auth.GetList("Role_ID in " + RoleIDs);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    string Btns = "{";
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        Btns += ds.Tables[0].Rows[i]["Button_ids"];
                    }
                    Btns.Replace(",,,,,,", "");
                    //Btns = Btns.Substring(0, Btns.Length - 1);
                    Btns += "}";
                    //Btns = Btns.Replace("b", "");
                    return Btns;
                }
                else
                {
                    return "(0)";
                }
            }
        }
    }
}