﻿/*
* CRM_Follow.cs
*
* 功 能： N/A
* 类 名： CRM_Follow
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2015-06-22 16:33:12    黄润伟    
*
* Copyright (c) 2015 www.xhdcrm.com   All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：黄润伟                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/

using System;

namespace XHD.Model
{
    /// <summary>
    ///     CRM_Follow:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public class CRM_Follow
    {
        #region Model

        private int? _contact_id;
        private int? _customer_id;
        private DateTime? _delete_time;
        private int? _department_id;
        private int? _employee_id;
        private string _follow;
        private int? _follow_aim_id;
        private DateTime? _follow_date;
        private int? _follow_type_id;
        private int _id;
        private int? _isdelete;

        /// <summary>
        /// </summary>
        public int id
        {
            set { _id = value; }
            get { return _id; }
        }

        /// <summary>
        /// </summary>
        public int? Customer_id
        {
            set { _customer_id = value; }
            get { return _customer_id; }
        }

        /// <summary>
        /// </summary>
        public int? Contact_id
        {
            set { _contact_id = value; }
            get { return _contact_id; }
        }

        /// <summary>
        /// </summary>
        public int? Follow_aim_id
        {
            set { _follow_aim_id = value; }
            get { return _follow_aim_id; }
        }

        /// <summary>
        /// </summary>
        public string Follow
        {
            set { _follow = value; }
            get { return _follow; }
        }

        /// <summary>
        /// </summary>
        public DateTime? Follow_date
        {
            set { _follow_date = value; }
            get { return _follow_date; }
        }

        /// <summary>
        /// </summary>
        public int? Follow_Type_id
        {
            set { _follow_type_id = value; }
            get { return _follow_type_id; }
        }

        /// <summary>
        /// </summary>
        public int? department_id
        {
            set { _department_id = value; }
            get { return _department_id; }
        }

        /// <summary>
        /// </summary>
        public int? employee_id
        {
            set { _employee_id = value; }
            get { return _employee_id; }
        }

        /// <summary>
        /// </summary>
        public int? isDelete
        {
            set { _isdelete = value; }
            get { return _isdelete; }
        }

        /// <summary>
        /// </summary>
        public DateTime? Delete_time
        {
            set { _delete_time = value; }
            get { return _delete_time; }
        }

        #endregion Model
    }
}