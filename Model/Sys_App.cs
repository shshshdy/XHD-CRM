/*
* Sys_App.cs
*
* 功 能： N/A
* 类 名： Sys_App
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2015-06-23 18:38:21    黄润伟    
*
* Copyright (c) 2015 www.xhdcrm.com   All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：黄润伟                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/

using System;

namespace XHD.Model
{
    /// <summary>
    ///     Sys_App:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public class Sys_App
    {
        #region Model

        private string _app_handler;
        private string _app_icon;
        private string _app_name;
        private int? _app_order;
        private string _app_type;
        private string _app_url;
        private int _id;

        /// <summary>
        /// </summary>
        public int id
        {
            set { _id = value; }
            get { return _id; }
        }

        /// <summary>
        /// </summary>
        public string App_name
        {
            set { _app_name = value; }
            get { return _app_name; }
        }

        /// <summary>
        /// </summary>
        public int? App_order
        {
            set { _app_order = value; }
            get { return _app_order; }
        }

        /// <summary>
        /// </summary>
        public string App_url
        {
            set { _app_url = value; }
            get { return _app_url; }
        }

        /// <summary>
        /// </summary>
        public string App_handler
        {
            set { _app_handler = value; }
            get { return _app_handler; }
        }

        /// <summary>
        /// </summary>
        public string App_type
        {
            set { _app_type = value; }
            get { return _app_type; }
        }

        /// <summary>
        /// </summary>
        public string App_icon
        {
            set { _app_icon = value; }
            get { return _app_icon; }
        }

        #endregion Model
    }
}