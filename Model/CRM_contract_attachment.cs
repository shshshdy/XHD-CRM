/*
* CRM_contract_attachment.cs
*
* 功 能： N/A
* 类 名： CRM_contract_attachment
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2015-06-23 18:38:21    黄润伟    
*
* Copyright (c) 2015 www.xhdcrm.com   All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：黄润伟                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/

using System;

namespace XHD.Model
{
    /// <summary>
    ///     CRM_contract_attachment:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public class CRM_contract_attachment
    {
        #region Model

        private int? _contract_id;
        private DateTime? _create_date;
        private int? _create_id;
        private string _create_name;
        private string _file_id;
        private string _file_name;
        private int? _file_size;
        private string _page_id;
        private string _real_name;

        /// <summary>
        /// </summary>
        public int? contract_id
        {
            set { _contract_id = value; }
            get { return _contract_id; }
        }

        /// <summary>
        /// </summary>
        public string page_id
        {
            set { _page_id = value; }
            get { return _page_id; }
        }

        /// <summary>
        /// </summary>
        public string file_id
        {
            set { _file_id = value; }
            get { return _file_id; }
        }

        /// <summary>
        /// </summary>
        public string file_name
        {
            set { _file_name = value; }
            get { return _file_name; }
        }

        /// <summary>
        /// </summary>
        public string real_name
        {
            set { _real_name = value; }
            get { return _real_name; }
        }

        /// <summary>
        /// </summary>
        public int? file_size
        {
            set { _file_size = value; }
            get { return _file_size; }
        }

        /// <summary>
        /// </summary>
        public int? create_id
        {
            set { _create_id = value; }
            get { return _create_id; }
        }

        /// <summary>
        /// </summary>
        public string create_name
        {
            set { _create_name = value; }
            get { return _create_name; }
        }

        /// <summary>
        /// </summary>
        public DateTime? create_date
        {
            set { _create_date = value; }
            get { return _create_date; }
        }

        #endregion Model
    }
}